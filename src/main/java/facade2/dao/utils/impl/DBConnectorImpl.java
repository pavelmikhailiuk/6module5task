package facade2.dao.utils.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;

import facade2.dao.exception.DAOException;
import facade2.dao.utils.DBConnector;

class DBConnectorImpl implements DBConnector {
	private ResourceBundle dbConfig = ResourceBundle.getBundle("jdbc");

	public Connection getConnection() throws DAOException {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(dbConfig.getString("url"), dbConfig.getString("user"),
					dbConfig.getString("password"));
		} catch (SQLException e) {
			throw new DAOException("Can't connect to database", e);
		}
		return connection;
	}
}