package facade2.dao.utils;

import java.sql.Connection;

import facade2.dao.exception.DAOException;

public interface DBConnector {
	Connection getConnection() throws DAOException;
}
